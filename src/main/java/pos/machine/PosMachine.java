package pos.machine;

import java.util.ArrayList;
import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }


    public List<ReceiptItem> decodeToItems(List<String> barcodes) {

        List<Item> items = ItemsLoader.loadAllItems(barcodes);

        int itemCount1 = 0;
        int itemCount2 = 0;
        int itemCount3 = 0;

        for (Item item : items) {
            if (item.getBarcode().equals("ITEM000000")) {
                itemCount1 = itemCount1 + 1;
            }
            else if (item.getBarcode().equals("ITEM000001")) {
                itemCount2 = itemCount2 + 1;
            }
            else {
                itemCount3 = itemCount3 + 1;
            }
        }

        ReceiptItem receiptItem1 = new ReceiptItem("ITEM000000", itemCount1, 3);
        ReceiptItem receiptItem2 = new ReceiptItem("ITEM000001", itemCount2, 3);
        ReceiptItem receiptItem3 = new ReceiptItem("ITEM000004", itemCount3, 2);

        List<ReceiptItem> receiptItems = new ArrayList<>();
        receiptItems.add(receiptItem1);
        receiptItems.add(receiptItem2);
        receiptItems.add(receiptItem3);
        return receiptItems;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        int totalPrice = calculateTotalPrice(receiptItems);
        return new Receipt(receiptItems, totalPrice);
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for(ReceiptItem receiptitem:receiptItems){
            totalPrice = totalPrice + receiptitem.getSubTotal();
        }
        return totalPrice;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }

    public String generateItemsReceipt(Receipt receipt) {
        String itemsReceipt = String.format("Name: Coca-Cola, Quantity: %d, Unit price: 3 (yuan), Subtotal: %d (yuan)\n", receipt.getReceiptItems().get(0).getQuantity(), receipt.getReceiptItems().get(0).getSubTotal());
        itemsReceipt = itemsReceipt + String.format("Name: Sprite, Quantity: %d, Unit price: 3 (yuan), Subtotal: %d (yuan)\n", receipt.getReceiptItems().get(1).getQuantity(), receipt.getReceiptItems().get(1).getSubTotal());
        itemsReceipt = itemsReceipt + String.format("Name: Battery, Quantity: %d, Unit price: 2 (yuan), Subtotal: %d (yuan)\n", receipt.getReceiptItems().get(2).getQuantity(), receipt.getReceiptItems().get(2).getSubTotal());
        return itemsReceipt;
    }

    public String generateReceipt(String itemsReceipt, int totalPrice){
        return "***<store earning no money>Receipt***\n" + itemsReceipt + "----------------------\n" + String.format("Total: %d (yuan)\n", totalPrice) + "**********************";
    }
}

