O(Objective):
+ Learned the constituent elements and drawing techniques of a context map.
+ I have learned the tasking and learned to decompose a task, gradually clarifying the completion steps from requirements to coding. 
+ I practiced drawing a context map.
+ Completed the process from requirements to code implementation. Reviewed the git instruction.&nbsp;And gradually submit the results using the git command when the exercise project is completed.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp;Due to limited learning of Java and lack of proficiency in programming syntax, I encountered difficulties and slow progress in the afternoon program implementation.&nbsp;With the assistance of search engines, code debugging was ultimately completed.&nbsp;At the same time, I am not very familiar with the git command. 

D(Decision):  
&emsp;&emsp;Two aspects above are the areas that I will focus on learning in the future.
